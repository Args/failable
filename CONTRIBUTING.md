# CONTRIBUTING

I don't imagine my work ever becoming popular enough that people might want to contribute.
If you wish to make the project better or add a feature, please make a PR.
If for whatever reason we can't agree on something, feel free to fork my work instead.
I would consider any form of participation as an honor.