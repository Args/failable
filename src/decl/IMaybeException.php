<?hh //strict

/**
 * @package Failable
 * @author Args Anderson | ArgsAnderson@protonmail.com
 * @date 08/Jan/2019
 */

namespace Args\Failable;

interface IMaybeException<T> {
    require extends \Exception;
    public function getMaybe(): IMaybe<T>;
}
