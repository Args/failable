<?hh //strict

/**
 * @package Failable
 * @author Args Anderson | ArgsAnderson@protonmail.com
 * @date 08/Jan/2019
 */

namespace Args\Failable;

/**
 * Wraps a value that may or may not exist.
 * Please use IMaybe<T> or as a return type for your functions.
 */
class Maybe<T> implements IMaybe<T>, Chainable {

    private ?Chainable $chained;

    private function __construct(private ?T $value, private bool $isset) {}

    /**
     * Constructs a new full Maybe.
     * @param T $value
     * @return Maybe<T>
     */
    public static function full(T $value): Maybe<T> {
        return new self($value, true);
    }

    /**
     * Constructs an empty Maybe of an unresolved type T.
     * @return Maybe<[unresolved]>
     */
    public static function empty<Tv>(): Maybe<T> {
        return new self<Tv>(null, false);
    }

    /**
     * Wraps a different IMaybe type to a Maybe<T>.
     */
    public static function wrap(IMaybe<T> $old): Maybe<T> {
        if ($old->isset()) {
            return self::full($old->getValue());
        }
        return self::empty();
    }

    /**
     * Determine if it is safe to get the value.
     * @return bool
     */
    public function isset(): bool {
        return $this->isset;
    }

    /**
     * Gets the value or throw if the value did not exist.
     */
    public function getValue(): T {
        if (!$this->isset) {
            throw new MaybeException("Maybe<T> was empty", $this);
        }
        /*HH_IGNORE_ERROR[4110]
        We know that the T must be a nullable type if it was constructed in ::full using a null.
        This means that T will always match $this->value unless isset is false.*/
        return $this->value;
    }

    /**
     * Chains the previous Chainable.
     */
    public function chain(Chainable $old): this {
        $this->chained = $old;
        return $this;
    }
}
