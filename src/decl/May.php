<?hh //strict

/**
 * @package Failable
 * @author Args Anderson | ArgsAnderson@protonmail.com
 * @date 08/Jan/2019
 */

namespace Args\Failable;

/**
 * Acts like a Maybe<T>, but also store an exception.
 * Please use IMaybe<T> or IMay<T> as a return type for your functions.
 */
class May<T> implements IMay<T>, Chainable {

    /**
     * For debugging where a chainable started.
     */
    private ?Chainable $chained;

    private function __construct(
        private IMaybe<T> $maybe,
        private ?\Exception $error,
    ) {}

    /**
     * Constructs a full May with the type T.
     * @param T $value
     * @return May<T>
     */
    public static function full(T $value): May<T> {
        return new self(Maybe::full($value), null);
    }

    /**
     * Constructs an empty May with a type of [unresolved]
     * @return May<[unresolved]>
     */
    public static function empty(\Exception $error): May<T> {
        return new self(Maybe::empty(), $error);
    }

    /**
     * Wraps a IMaybe to become a May<T>.
     * Will error if the exception is not needed or missing.
     * @param IMaybe<T>
     * @param ?\Exception [optional] $error
     * @return May<T>
     */
    public static function wrap(
        IMaybe<T> $maybe,
        ?\Exception $error = null,
    ): May<T> {
        if (!$maybe->isset() && $error === null) {
            \trigger_error(
                "Attempted to wrap without using an exception when the value was not set. A default exception will be used.",
            );
            $error = new \RuntimeException(
                "Attempted to wrap without using an exception when the value was not set.",
            );
        }
        if ($maybe->isset() && $error !== null) {
            \trigger_error(
                'Attempted to wrap a succesful IMaybe<T> as a failure. This indicates incorrect error checking'.
                \var_export(\debug_backtrace(), true),
                \E_USER_WARNING,
            );
            $error = null;
        }
        $self = new self($maybe, $error);
        if ($maybe is Chainable) {
            $self->chain($maybe);
        }
        return $self;
    }

    /**
     * Checks if the value exists.
     * @return bool
     */
    public function isset(): bool {
        return $this->maybe->isset();
    }

    /**
     * Gets the value if it exists, else it throws a MayException.
     */
    public function getValue(): T {
        if (!$this->maybe->isset()) {
            throw new MayException("May<T> was empty", $this);
        }
        return $this->maybe->getValue();
    }

    public function getException(): ?\Exception {
        return $this->error;
    }

    /**
     * Allow for instanceof checking instead of throwing and catching it immediately.
     * If no exception exists, a MayException will be thrown.
     * @throws MayException
     */
    public function getExceptionReq(): \Exception {
        if ($this->isset()) {
            throw new MayException("Requested a non existant exception", $this);
        }
        //We know this won't fail.
        return $this->error as \Exception;
    }

    /**
     * Throws an new MayException<T> if the maybe is not set.
     */
    public function throwIfExists(): this {
        if ($this->maybe->isset()) {
            return $this;
        }
        throw new MayException("Thrown on demand", $this);
    }

    /**
     * Throws the exception given to it at construct if the maybe is not set.
     * @throws \Exception
     */
    public function throwInnerIfExists(): this {
        if ($this->maybe->isset()) {
            return $this;
        }
        //We know this to never fail.
        throw $this->error as \Exception;
    }

    /**
     * Chains the previous Chainable.
     */
    public function chain(Chainable $old): this {
        $this->chained = $old;
        return $this;
    }
}
