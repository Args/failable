<?hh //strict

/**
 * @package Failable
 * @author Args Anderson | ArgsAnderson@protonmail.com
 * @date 08/Jan/2019
 */

namespace Args\Failable;

/**
 * Maybe-like
 */
interface IMaybe<T> {
    public function isset(): bool;
    public function getValue(): T;
}
