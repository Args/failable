<?hh //strict

/**
 * @package Failable
 * @author Args Anderson | ArgsAnderson@protonmail.com
 * @date 08/Jan/2019
 */

namespace Args\Failable;

/**
 * Allows from IMaybe and May to chain without having a matching T.
 */
interface Chainable {
    public function chain(Chainable $old): this;
}
