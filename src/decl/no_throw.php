<?hh //strict

/**
 * @package Failable
 * @author Args Anderson | ArgsAnderson@protonmail.com
 * @date 08/Jan/2019
 */

namespace Args\Failable;

/**
 * Executes a function that may throw any exceptions and turn them into a May<T>.
 * It is recommended to do this with a lambda,
 * since if you have to wrap this around a normal function that throws something unknown,
 * your log file will be filled with warnings about undocumented exceptions.
 */
function no_throw<T>((function(): T) $callable): IMaybe<T> {
    try {
        return Maybe::full($callable());
    } catch (IMaybeException $e) {
        return $e->getMaybe();
    } catch (\Exception $e) {
        \trigger_error(
            "Exception thrown in a no-throw context\n".$e,
            \E_USER_WARNING,
        );
        return Maybe::empty();
    }
}
