<?hh //strict

/**
 * @package Failable
 * @author Args Anderson | ArgsAnderson@protonmail.com
 * @date 08/Jan/2019
 */

namespace Args\Failable;

/**
 * A placeholder exception for when you know nothing about what went wrong.
 */
class UnknownException extends \Exception {}
