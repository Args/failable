<?hh //strict

/**
 * @package Failable
 * @author Args Anderson | ArgsAnderson@protonmail.com
 * @date 08/Jan/2019
 */

namespace Args\Failable;

/**
 * Throws a Maybe to the nearest try catch.
 * Ought not to cross function scopes.
 */
class MaybeException<T> extends \Exception implements IMaybeException<T> {

    public function __construct(string $message, private Maybe<T> $maybe) {
        parent::__construct($message);
    }

    public function getMaybe(): Maybe<T> {
        return $this->maybe;
    }
}
