<?hh //strict

/**
 * @package Failable
 * @author Args Anderson | ArgsAnderson@protonmail.com
 * @date 08/Jan/2019
 */

namespace Args\Failable;

/**
 * Throws a May to the nearest try catch.
 * Ought not to cross function scopes.
 */
class MayException<T> extends \Exception implements IMaybeException<T> {

    public function __construct(string $message, private May<T> $maybe) {
        parent::__construct($message);
    }

    public function getMaybe(): May<T> {
        return $this->maybe;
    }
}
