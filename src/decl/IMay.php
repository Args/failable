<?hh //strict

/**
 * @package Failable
 * @author Args Anderson | ArgsAnderson@protonmail.com
 * @date 08/Jan/2019
 */

namespace Args\Failable;

/**
 * Add exception capabilities.
 */
interface IMay<T> extends IMaybe<T> {

    /**
    * Throws a IMayException<T> if the exception does not exist.
    * @throws IMayException<T>
    */
    public function getException(): ?\Exception;
    /**
     * Throws a IMayException<T> if the exception does not exist.
     * @throws IMayException<T>
     */
    public function getExceptionReq(): \Exception;
    /**
     * Throws a IMayException<T> if it is not set.
     * @throws IMayException<T>
     */
    public function throwIfExists(): this;
    /**
     * Throws the exception with was given to it in the constructor if it exists.
     * @throws \Exception
     */
    public function throwInnerIfExists(): this;
}
